package spriteEdit;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import spriteEdit.gui.Window;

public class SpriteEdit
{
	public static void main(String[] a)
	{
		Model m = new Model();
		try {
			// Set System L&F
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (UnsupportedLookAndFeelException e) {
			// handle exception
		} catch (ClassNotFoundException e) {
			// handle exception
		} catch (InstantiationException e) {
			// handle exception
		} catch (IllegalAccessException e) {
			// handle exception
		}

		Window w = new Window(m);
	}
}