package spriteEdit.commands;


import java.awt.image.DataBufferByte;

import spriteEdit.Model;
import spriteEdit.interfaces.ITileCommand;

public class SetTileCommand implements ITileCommand
{
	private Model model;
	private int tilex = 0;
	private int tiley = 0;
	
	public SetTileCommand(Model model)
	{
		this.model = model;
	}
	
	public void Execute()
	{
		byte[] pixelst = ((DataBufferByte) model.GetTiles().getRaster().getDataBuffer()).getData();
		byte[] pixelsb = model.GetBitmap();
		
		for (int i = 0; i < 32; i++) {
			for (int j = 0; j < 32; j++) {
				int x0 = 32 * tilex + j;
				int y0 = 32 * tiley + i;
				// pixelsb[i * 32 + j] = (byte)();
				pixelst[y0 * 512 + x0] = (byte) (pixelsb[i * 32 + j]);
			}
		}
	}

	public void Undo()
	{
		// TODO Auto-generated method stub
		
	}

	public void SetTile(int x, int y)
	{
		tilex = x;
		tiley = y;
		
	}

}
