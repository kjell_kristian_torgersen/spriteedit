package spriteEdit.commands;


import spriteEdit.interfaces.ICommand;

public final class UnknownCommand implements ICommand
{
	private final String message;

	public UnknownCommand(String message)
	{
		this.message = message;
	}

	public void Execute()
	{
		System.out.println(message);	
	}
}
