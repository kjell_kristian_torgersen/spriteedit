package spriteEdit.commands;


import spriteEdit.Model;
import spriteEdit.interfaces.IUndoCommand;

public class ShiftCommand implements IUndoCommand
{
	private Model model;
	
	private int dx;
	private int dy;
	
	public ShiftCommand(Model model, int dx, int dy)
	{
		this.model = model;
		this.dx = dx;
		this.dy = dy;
	}
	
	private void shift(int x, int y)
	{
		byte[] pixelstmp = new byte[32 * 32];
		for (int i = 0; i < 32; i++) {
			for (int j = 0; j < 32; j++) {
				pixelstmp[i * 32 + j] = model.GetPixel((j + x) & 31, (i + y) & 31);
			}
		}
		for (int i = 0; i < 32; i++) {
			for (int j = 0; j < 32; j++) {
				model.SetPixel(j, i, pixelstmp[i * 32 + j]);
			}
		}
	}
	public void Execute()
	{
		shift(dx, dy);
	}

	public void Undo()
	{
		shift(-dx, -dy);

	}

}
