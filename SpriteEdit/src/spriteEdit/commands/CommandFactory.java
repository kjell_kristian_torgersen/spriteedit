package spriteEdit.commands;

import spriteEdit.Model;
import spriteEdit.interfaces.ICommand;

public class CommandFactory
{

	public static ICommand GetClearCommand(Model model)
	{
		return new ClearCommand(model);
	}

	public static ICommand GetFloodFillCommand(Model model, int x, int y, byte oldcolor, byte newcolor)
	{
		return new FloodFillCommand(model, x, y, oldcolor, newcolor);
	}

	public static ICommand GetGetTileCommand(Model model)
	{
		return new GetTileCommand(model);
	}

	public static ICommand GetSetPixelCommand(Model model, int x, int y, byte color)
	{
		return new SetPixelCommand(model, x, y, color);
	}
	
	public static ICommand GetSetTileCommand(Model model)
	{
		return new SetTileCommand(model);
	}
	
	public static ICommand GetShiftCommand(Model model, int dx, int dy)
	{
		return new ShiftCommand(model, dx, dy);
	}
	
	public static ICommand GetUnknownCommand(String message)
	{
		return new UnknownCommand(message);
	}
}
