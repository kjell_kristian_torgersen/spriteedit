package spriteEdit.commands;


import spriteEdit.Model;
import spriteEdit.interfaces.IUndoCommand;

public class SetPixelCommand implements IUndoCommand
{
	private Model model;
	private int x;
	private int y;
	private byte color;
	private byte undocolor;
	
	public SetPixelCommand(Model model, int x, int y, byte color)
	{
		this.model = model;
		this.x = x;
		this.y = y;
		this.color = color;
	}

	public void Execute()
	{
		undocolor = model.GetPixel(x, y);
		model.DrawPixel(x, y, color);

	}

	public void Undo()
	{
		model.SetPixel(x, y, undocolor);
		
	}

}
