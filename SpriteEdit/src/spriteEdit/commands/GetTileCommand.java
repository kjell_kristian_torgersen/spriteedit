package spriteEdit.commands;


import java.awt.image.DataBufferByte;

import spriteEdit.Model;
import spriteEdit.interfaces.ITileCommand;

public class GetTileCommand implements ITileCommand
{
	private Model model;
	private byte[] undoimage;
	private int tilex=0;
	private int tiley=0;
	public GetTileCommand(Model model)
	{
		this.model = model;
		this.undoimage = new byte[32*32];
	}

	public void Execute()
	{
		byte[] pixelst = ((DataBufferByte) model.GetTiles().getRaster().getDataBuffer()).getData();

		for (int i = 0; i < 32; i++) {
			for (int j = 0; j < 32; j++) {
				int x0 = 32 * tilex + j;
				int y0 = 32 * tiley + i;
				// pixelsb[i * 32 + j] = (byte)();
				undoimage[i*32+j] = model.GetPixel(j, i);
				model.SetPixel(j, i, pixelst[y0 * 512 + x0]);
			}
		}

	}

	public void Undo()
	{
		for (int i = 0; i < 32; i++) {
			for (int j = 0; j < 32; j++) {
				// pixelsb[i * 32 + j] = (byte)();
				//undoimage[i*32+j] = atelier.painting.GetPixel(j, i);
				model.SetPixel(j, i, undoimage[i * 32 + j]);
			}
		}

	}

	public void SetTile(int x, int y)
	{
		tilex = x;
		tiley = y;
		
	}

}
