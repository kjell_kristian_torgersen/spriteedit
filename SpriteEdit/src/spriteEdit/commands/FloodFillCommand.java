package spriteEdit.commands;


import spriteEdit.Model;
import spriteEdit.interfaces.IUndoCommand;

public class FloodFillCommand implements IUndoCommand
{
	private Model model;
	private int x;
	private int y;
	private byte[] undocolors;
	private byte newcolor;
	private byte oldcolor;

	public FloodFillCommand(Model model, int x, int y, byte oldcolor, byte newcolor)
	{
		this.model = model;
		this.x = x;
		this.y = y;
		this.oldcolor = oldcolor;
		this.newcolor = newcolor;
		this.undocolors = new byte[32 * 32];
		for(int i = 0; i < 32*32; i++) this.undocolors[i] = -1;
	}
	
	void floodFill(int x, int y)
	{
		if (x < 0 || x >= 32)
			return;
		if (y < 0 || y >= 32)
			return;
		if(undocolors[y*32+x] != -1) return;
		if (model.GetPixel(x, y) != oldcolor)
			return;
		if (newcolor == oldcolor)
			return;
		
		
		undocolors[y*32+x] = model.GetPixel(x, y);
		model.DrawPixel(x, y, newcolor);
		floodFill(x - 1, y);
		floodFill(x + 1, y);
		floodFill(x, y - 1);
		floodFill(x, y + 1);

	}

	public void Execute()
	{
		floodFill(x, y);
	}

	public void Undo()
	{
		for (int i = 0; i < 32; i++) {
			for (int j = 0; j < 32; j++) {
				if(undocolors[i*32+j] != -1) model.SetPixel(j, i, undocolors[i*32+j]);
			}
		}

	}

}
