package spriteEdit.commands;


import spriteEdit.Model;
import spriteEdit.interfaces.IUndoCommand;

public class ClearCommand implements IUndoCommand
{
	private Model model;
	private byte[] undocolors;

	public ClearCommand(Model model)
	{
		this.model = model;
		this.undocolors = new byte[32 * 32];
	}
	
	
	public void Execute()
	{
		for(int i = 0; i < 32; i++) {
			for(int j = 0; j < 32; j++) {
				undocolors[i*32+j] = model.GetPixel(j, i);
				model.SetPixel(j, i, (byte) 16);
			}
		}
	}

	public void Undo()
	{
		for (int i = 0; i < 32; i++) {
			for (int j = 0; j < 32; j++) {
				model.SetPixel(j, i,undocolors[i*32+j]);
			}
		}

	}

}
