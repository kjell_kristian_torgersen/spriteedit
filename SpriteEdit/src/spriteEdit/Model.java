package spriteEdit;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;
import java.io.IOException;
import java.util.Stack;

import javax.imageio.ImageIO;

import spriteEdit.gui.patterns.NormalPattern;
import spriteEdit.interfaces.IPencil;
import spriteEdit.interfaces.ISprite;
import spriteEdit.interfaces.IUndoCommand;

public class Model
{
	private ISprite sprite;
	private BufferedImage tiles;
	private Palette palette;
	private IPencil pencil;

	private Stack<IUndoCommand> history = new Stack<IUndoCommand>();
	
	public Model()
	{
		palette = new Palette();
		tiles = new BufferedImage(512, 512, BufferedImage.TYPE_BYTE_INDEXED, palette.GetColorModel());

		byte[] pixels = ((DataBufferByte) tiles.getRaster().getDataBuffer()).getData();

		for (int i = 0; i < 512 * 512; i++) {
			pixels[i] = 16;
		}

		try {
			BufferedImage tilesTmp = ImageIO.read(new File("tiles.png"));
			tiles.createGraphics().drawImage(tilesTmp, 0, 0, null);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		sprite = new Sprite(32, 32, palette.GetColorModel());

		pencil = new NormalPattern(sprite);	
	}
	
	public ISprite GetSprite()
	{
		return sprite;
	}
	
	public Palette GetPalette()
	{
		return palette;
	}
	
	public byte[] GetBitmap()
	{
		return sprite.GetBitmap();
	}
	
	public void DrawPixel(int x, int y, byte color)
	{
		pencil.Draw(x, y, color);
	}
	
	public void SetPixel(int x, int y, byte color)
	{
		sprite.SetPixel(x, y, color);
	}
	
	public void StoreAndExecute(IUndoCommand command)
	{
		history.add(command);
		command.Execute();
	}

	public byte GetPixel(int x, int y)
	{
		return sprite.GetPixel(x, y);
	}

	public void Undo()
	{
		if (!history.empty())
			history.pop().Undo();
		
	}
	
	public void SetPencil(IPencil pencil) 
	{
		this.pencil = pencil;
	}
	
	/*public void SetPencil(Pattern mode)
	{
		this.pencil = PatternFactory.Create(mode, sprite);
	}*/
	
	public BufferedImage GetImage()
	{
		return sprite.GetImage();
	}
	
	public BufferedImage GetIsometricImage(boolean b)
	{
		return sprite.GetIsometricImage(b);
	}
	public BufferedImage GetTiles()
	{ 
		return tiles;
	}
}
