package spriteEdit.interfaces;

public interface IUndoCommand extends ICommand
{
	void Undo();
}
