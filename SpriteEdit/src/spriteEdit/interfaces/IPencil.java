package spriteEdit.interfaces;

public interface IPencil
{
	byte Peek(int x, int y);
	void Draw(int x, int y, byte color);
}
