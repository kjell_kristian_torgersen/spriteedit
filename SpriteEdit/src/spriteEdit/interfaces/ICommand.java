package spriteEdit.interfaces;

public interface ICommand
{
	void Execute();
}
