package spriteEdit.interfaces;

import java.awt.image.BufferedImage;

public interface ISprite
{

	public abstract void SetPixel(int x, int y, byte color);

	public abstract byte[] GetBitmap();

	public abstract BufferedImage GetImage();

	public abstract BufferedImage GetIsometricImage(Boolean angle);

	public abstract byte GetPixel(int x, int y);

	public abstract void Clear();

	//public abstract void LoadTile(int tilex, int tiley);

}