package spriteEdit.interfaces;

import java.awt.Graphics;

public interface IDrawable
{
	void draw(Graphics g, int x, int y);
}
