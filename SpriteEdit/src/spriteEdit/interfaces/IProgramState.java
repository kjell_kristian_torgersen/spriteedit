package spriteEdit.interfaces;

import java.awt.Graphics;
import java.awt.event.MouseEvent;

public interface IProgramState
{
	public void Click(MouseEvent e);
	public void MouseMoved(MouseEvent e);
	public void Draw(Graphics g);
}
