package spriteEdit.interfaces;


public interface ITileCommand extends IUndoCommand
{
	void SetTile(int x, int y);
}
