package spriteEdit.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import spriteEdit.Model;
import spriteEdit.commands.ClearCommand;
import spriteEdit.commands.CommandFactory;
import spriteEdit.commands.FloodFillCommand;
import spriteEdit.commands.GetTileCommand;
import spriteEdit.commands.SetPixelCommand;
import spriteEdit.commands.SetTileCommand;
import spriteEdit.commands.ShiftCommand;
import spriteEdit.gui.patterns.Pattern;
import spriteEdit.gui.patterns.PatternFactory;
import spriteEdit.gui.states.DrawState;
import spriteEdit.gui.states.SelectTileState;
import spriteEdit.interfaces.IProgramState;

public class GUI extends JPanel implements MouseListener, MouseMotionListener, ActionListener
{
	private static final long serialVersionUID = 1L;

	private final int pixelSize = 16;
	private final int paletteSize = 32;

	private String infotext = "TestTestTest";
	private String filename = "tiles.png";

	private byte[] colors;
	private byte currentColor = 0;
	private Model model;

	private IProgramState guiState;

	public void SetPrimaryColor(byte color) 
	{
		colors[0] = color;
	}
	
	public void SetSecondaryColor(byte color) 
	{
		colors[1] = color;
	}
	
	public byte GetPrimaryColor() 
	{
		return colors[0];
	}
	
	public byte GetSecondaryColor() 
	{
		return colors[1];
	}
	
	public GUI(Model model)
	{

		setFocusable(true);
		addMouseListener(this);
		addMouseMotionListener(this);
		setPreferredSize(new Dimension(640, 480));
		setBackground(Color.GRAY);

		guiState = new DrawState(this);

		colors = new byte[2];
		colors[0] = 0;
		colors[1] = 16;
		//currentColor = colors[0];
		this.model = model;

	}

	public void paint(Graphics g)
	{
		guiState.Draw(g);

	}

	public void drawPixel(MouseEvent e)
	{
		int x, y;
		// System.out.println(e.getButton());
		//if (mode == 0) {
			if (e.getX() >= paletteSize && (e.getX() < (paletteSize + 32 * pixelSize))) {
				if ((e.getY() >= 0) && (e.getY() < 32 * pixelSize)) {
					x = (e.getX() - paletteSize) / pixelSize;
					y = e.getY() / pixelSize;

					switch (e.getButton()) {
					case MouseEvent.BUTTON1:
						/*
						 * pencil.Draw(x, y,
						 * currentColor);
						 */
						//currentColor = colors[0];
						model.StoreAndExecute(new SetPixelCommand(model, x, y, currentColor));
						break;
					case MouseEvent.BUTTON2:
						//currentColor = colors[0];
						model.StoreAndExecute(new FloodFillCommand(model, x, y, model.GetPixel(x, y), colors[0]));
						// floodFill(x, y, currentColor,
						// painting.GetPixel(x, y));
						break;
					case MouseEvent.BUTTON3:
						//currentColor = colors[1];
						model.StoreAndExecute(new SetPixelCommand(model, x, y, currentColor));
						break;
					default:
						model.StoreAndExecute(new SetPixelCommand(model, x, y, currentColor));
						break;
					}
				}
			}
		//}
	}

	public void mouseClicked(MouseEvent e)
	{
		guiState.Click(e);
		repaint();

	}

	public void mousePressed(MouseEvent e)
	{
		switch (e.getButton()) {
		case MouseEvent.BUTTON1:
			currentColor = colors[0];
			break;
		case MouseEvent.BUTTON2:
			currentColor = colors[0];
			break;
		case MouseEvent.BUTTON3:
			currentColor = colors[1];
			break;
		default:
			break;
		}
		repaint();

	}

	public void mouseReleased(MouseEvent e)
	{
		// TODO Auto-generated method stub

	}

	public void mouseEntered(MouseEvent e)
	{
		// TODO Auto-generated method stub

	}

	public void mouseExited(MouseEvent e)
	{
		// TODO Auto-generated method stub

	}

	public void mouseDragged(MouseEvent e)
	{
		// TODO Auto-generated method stub
		drawPixel(e);
		repaint();
	}

	public void mouseMoved(MouseEvent e)
	{
		int px = (e.getX() - paletteSize) / pixelSize;
		int py = (e.getY()) / pixelSize;
		infotext = "[" + px + ", " + py + "]";

		guiState.MouseMoved(e);

		repaint();
	}

	public void actionPerformed(ActionEvent e)
	{
		JMenuItem source = (JMenuItem) (e.getSource());

		if (source.getText() == "New") {
			// System.out.println(source.getText());
			CommandFactory.GetUnknownCommand("Unknown command: " + source.getText()).Execute();
		} else if (source.getText() == "Open file...") {
			CommandFactory.GetUnknownCommand("Unknown command: " + source.getText()).Execute();
		} else if (source.getText() == "Save") {

			try {
				ImageIO.write(model.GetTiles(), "png", new File(filename));
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			// System.out.println(source.getText());
		} else if (source.getText() == "Save as...") {
			System.out.println(source.getText());
		} else if (source.getText() == "Exit") {
			SwingUtilities.getWindowAncestor(this).dispose();
		} else if (source.getText() == "Clear") {
			model.StoreAndExecute(new ClearCommand(model));
		} else if (source.getText() == "Undo") {
			model.Undo();
		} else if (source.getText() == "Set tile") {
			guiState = new SelectTileState(model, this, Color.RED, new SetTileCommand(model));
		} else if (source.getText() == "Get tile") {
			guiState = new SelectTileState(model, this, Color.GREEN, new GetTileCommand(model));
		} else if (source.getText() == "Shift 16x16 pixels") {
			model.StoreAndExecute(new ShiftCommand(model, 16, 16));
		} else if (source.getText() == "Dither mode") {
			model.SetPencil(PatternFactory.Create(Pattern.DitherMode, model.GetSprite()));
			//model.pencil = new DitherPattern(model.painting);
		} else if (source.getText() == "Normal mode") {
			model.SetPencil(PatternFactory.Create(Pattern.NormalMode, model.GetSprite()));
		} else if (source.getText() == "Tile 8x8") {
			model.SetPencil(PatternFactory.Create(Pattern.TilePattern, model.GetSprite(),8,8));
		} else { //

		}
	}

	public void SetState(IProgramState programState)
	{
		this.guiState = programState;

	}

	public int GetPaletteSize()
	{
		return paletteSize;
	}

	public int GetPixelSize()
	{
		return pixelSize;
	}
	
	public String GetInfoText()
	{
		return infotext;
	}

	public Model GetModel()
	{
		return model;
	}

}
