package spriteEdit.gui.states;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;

import spriteEdit.gui.GUI;
import spriteEdit.interfaces.IProgramState;

public class DrawState implements IProgramState
{
	private GUI gUI;

	public DrawState(GUI gUI)
	{
		this.gUI = gUI;
	}

	public void Click(MouseEvent e)
	{
		int x = e.getX();
		if (x < gUI.GetPaletteSize()) {
			if (e.getY() < 17 * gUI.GetPaletteSize()) {
				switch (e.getButton()) {
				case MouseEvent.BUTTON1:
					gUI.SetPrimaryColor((byte) (e.getY() / gUI.GetPaletteSize()));
					break;
				case MouseEvent.BUTTON3:
					gUI.SetSecondaryColor((byte) (e.getY() / gUI.GetPaletteSize()));
					break;
				}
			}
		}

		gUI.drawPixel(e);

	}

	public void Draw(Graphics g)
	{
		int paletteSize = gUI.GetPaletteSize();
		int pixelSize = gUI.GetPixelSize();

		byte[] bitmap = gUI.GetModel().GetBitmap();
		g.clearRect(0, 0, gUI.getWidth(), gUI.getHeight());

		for (int i = 0; i < 17; i++) {
			g.setColor(gUI.GetModel().GetPalette().GetColor(i));
			g.fill3DRect(0, paletteSize * i, paletteSize, paletteSize, i != gUI.GetPrimaryColor());
		}

		g.setColor(gUI.GetModel().GetPalette().GetColor(7));

		for (int y = 0; y < 32; y++) {
			for (int x = 0; x < 32; x++) {
				g.setColor(Color.BLACK);
				g.drawRect(paletteSize + pixelSize * x, pixelSize * y, pixelSize, pixelSize);
				g.setColor(gUI.GetModel().GetPalette().GetColor(bitmap[x + 32 * y]));
				g.fill3DRect(paletteSize + pixelSize * x, pixelSize * y, pixelSize, pixelSize, true);
			}
		}

		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				g.drawImage(gUI.GetModel().GetImage(), paletteSize + 32 * pixelSize + i * 32, 64 + j * 32, gUI);
			}
		}
		// g.setColor(Color.PINK);

		g.drawImage(gUI.GetModel().GetIsometricImage(false), 32 + 32 * 16 + 3 * 32, 0, gUI);
		/*
		 * for (int i = 0; i < 3; i++) { int j = 0;
		 * g.drawImage(gUI.GetModel().GetIsometricImage(false),
		 * paletteSize + 32 * pixelSize + 3 * 32 + 32 * i, 32 + 16 - 16
		 * * i + 32 * j, gUI);
		 * 
		 * }
		 */

		int x0 = paletteSize + 32 * pixelSize;
		int y0 = 32 * 6;
		int x_;
		int y_;

		for (int y = 0; y < 32; y++) {
			for (int x = 0; x < 32; x++) {
				g.setColor(gUI.GetModel().GetPalette().GetColor(gUI.GetModel().GetPixel(x, y)));
				x_ = x + x0;
				y_ = y + y0 - x / 2;
				g.drawLine(x_, y_, x_, y_);
			}
		}

		g.setColor(Color.BLACK);
		g.drawString(gUI.GetInfoText(), 0, gUI.getHeight() - 1);

	}

	public void MouseMoved(MouseEvent e)
	{
		// TODO Auto-generated method stub

	}
}

/* 
 * 
 * 1 2 3
 * 4 5 6
 * 7 8 9
 * 
 *     3
 *   2   6 
 * 1   5   9
 *   4   8
 *     7
 * */
