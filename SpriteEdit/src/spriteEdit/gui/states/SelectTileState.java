package spriteEdit.gui.states;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;

import spriteEdit.Model;
import spriteEdit.gui.GUI;
import spriteEdit.interfaces.IProgramState;
import spriteEdit.interfaces.ITileCommand;

public class SelectTileState implements IProgramState
{
	private Model model;
	private Color selectColor;
	private ITileCommand tileCommand;
	private GUI gUI;
	private int tilex = 0;
	private int tiley = 0;

	public SelectTileState(Model model, GUI gUI, Color selectColor, ITileCommand tileCommand)
	{
		this.model = model;
		this.gUI = gUI;
		this.selectColor = selectColor;
		this.tileCommand = tileCommand;
	}

	public void Click(MouseEvent e)
	{
		int x = e.getX();
		int y = e.getY();

		tilex = x / 32;
		tiley = y / 32;

		if (tilex >= 15)
			tilex = 15;
		if (tiley >= 15)
			tiley = 15;
		
		tileCommand.SetTile(tilex, tiley);
		model.StoreAndExecute(tileCommand);
		gUI.SetState(new DrawState(gUI));

	}

	public void Draw(Graphics g)
	{
		char[] hex = new char[]{'0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f'};
		g.clearRect(0, 0, gUI.getWidth(), gUI.getHeight());
		g.drawImage(model.GetTiles(), 0, 0, gUI);
		g.setColor(selectColor);
		g.drawRect(32 * tilex, 32 * tiley, 32, 32);
		g.drawString("Sprite = 0x"+hex[tiley]+hex[tilex], 0, gUI.getHeight() - 1);

	}

	public void MouseMoved(MouseEvent e)
	{
		int x = e.getX();
		int y = e.getY();

		tilex = x / 32;
		tiley = y / 32;
		if (tilex >= 15)
			tilex = 15;
		if (tiley >= 15)
			tiley = 15;

	}

}
