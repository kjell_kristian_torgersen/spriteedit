package spriteEdit.gui.patterns;

import spriteEdit.interfaces.IPencil;
import spriteEdit.interfaces.ISprite;

public class PatternFactory
{
	
	public static IPencil Create(Pattern pattern, ISprite sprite)
	{
		return Create(pattern, sprite, 8,8);
	}
	
	public static IPencil Create(Pattern pattern, ISprite sprite, int x, int y)
	{
		IPencil result = null;
		switch (pattern) {
		case DitherMode:
			result = new DitherPattern(sprite);
			break;
		case NormalMode:
			result = new NormalPattern(sprite);
			break;
		case TilePattern:
			result = new TilePattern(sprite, x, y);
			break;

		default:
			break;
		}
		return result;
		
	}
}
