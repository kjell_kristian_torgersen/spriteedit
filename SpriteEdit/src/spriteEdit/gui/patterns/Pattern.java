package spriteEdit.gui.patterns;

public enum Pattern {
	DitherMode, NormalMode, TilePattern
}
