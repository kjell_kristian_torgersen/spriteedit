package spriteEdit.gui.patterns;

import spriteEdit.interfaces.ISprite;
import spriteEdit.interfaces.IPencil;

public class DitherPattern implements IPencil
{

	private ISprite sprite;

	public DitherPattern(ISprite sprite)
	{
		this.sprite = sprite;
	}

	public void Draw(int x, int y, byte color)
	{
		if (((x + y) & 1) == 0)
			sprite.SetPixel(x, y, color);
	}
	
	public byte Peek(int x, int y)
	{
		return sprite.GetPixel(x, y);
	}

}
