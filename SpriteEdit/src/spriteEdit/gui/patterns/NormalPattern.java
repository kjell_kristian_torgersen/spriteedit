package spriteEdit.gui.patterns;

import spriteEdit.interfaces.ISprite;
import spriteEdit.interfaces.IPencil;

public class NormalPattern implements IPencil
{

	private ISprite sprite;
	
	public NormalPattern(ISprite sprite)
	{
		this.sprite = sprite;
	}
	
	public void Draw(int x, int y, byte color)
	{
		// TODO Auto-generated method stub
		sprite.SetPixel(x, y, color);
	}

	public byte Peek(int x, int y)
	{
		return sprite.GetPixel(x, y);
	}
	

}
