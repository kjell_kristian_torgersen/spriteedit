package spriteEdit.gui.patterns;

import spriteEdit.interfaces.ISprite;
import spriteEdit.interfaces.IPencil;

public class TilePattern implements IPencil
{

	private ISprite sprite;
	private int sx;
	private int sy;
	
	public TilePattern(ISprite sprite, int x, int y)
	{
		this.sprite = sprite;
		this.sx = x;
		this.sy = y;
	}

	public void Draw(int x, int y, byte color)
	{
		if(((x % sx) == 0) || ((y % sy)==0))
			sprite.SetPixel(x, y, color);
	}
	
	public byte Peek(int x, int y)
	{
		return sprite.GetPixel(x, y);
	}

}
