package spriteEdit.gui;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import spriteEdit.Model;

public class Window extends JFrame
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6836289401718754227L;
	private GUI _atelier = null;
	
	public Window(Model m)
	{
		super();
		// _window = new JFrame();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		_atelier = new GUI(m);

		setJMenuBar(CreateMenuBar());

		getContentPane().add(_atelier);
		pack();
		setSize(640, 480);
		setVisible(true);

	}

	JMenu CreateMenu(String name, String[] text)
	{
		JMenu menu = new JMenu(name);
		JMenuItem item;
		for (int i = 0; i < text.length; i++) {
			item = new JMenuItem(text[i]);
			item.addActionListener(_atelier);
			menu.add(item);
		}

		return menu;
	}

	JMenuBar CreateMenuBar()
	{
		final String[] _file_text = { "New", "Open file...", "Save", "Save as...", "Exit" };
		final String[] _edit_text = { "Undo", "Clear", "Set tile", "Get tile", "Shift 16x16 pixels", "Dither mode", "Normal mode", "Tile 8x8" };

		JMenuBar menuBar = new JMenuBar();
		menuBar.add(CreateMenu("File", _file_text));
		menuBar.add(CreateMenu("Edit", _edit_text));
		return menuBar;
	}
}
