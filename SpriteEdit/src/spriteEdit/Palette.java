package spriteEdit;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.DataBuffer;
import java.awt.image.IndexColorModel;

import spriteEdit.interfaces.IDrawable;

public class Palette implements IDrawable
{
	private Color[] palette;
	private IndexColorModel colorModel;
	private final int[] colourMap = { 0xFF000000, 0xFF0000AA, 0xFF00AA00, 0xFF00AAAA, 0xFFAA0000, 0xFFAA00AA, 0xFFAA5500, 0xFFAAAAAA, 0xFF555555, 0xFF5555FF, 0xFF55FF55, 0xFF55FFFF,
			0xFFFF5555, 0xFFFF55FF, 0xFFFFFF00, 0xFFFFFFFF, 0x0 };
	
	public Palette()
	{
		colorModel = new IndexColorModel(8, colourMap.length, colourMap, 0, true, 16, DataBuffer.TYPE_BYTE);
		palette = new Color[17];

		for (int i = 0; i < 17; i++) {
			palette[i] = new Color(colourMap[i], true);
		}
	}
	
	public void draw(Graphics g, int x, int y)
	{
		// TODO Auto-generated method stub

	}

	public Color GetColor(int c) {
		return palette[c];
	}
	
	public IndexColorModel GetColorModel()
	{
		return colorModel;
	}

}
