package spriteEdit;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.IndexColorModel;

import spriteEdit.interfaces.ISprite;

public class Sprite implements ISprite
{
	private BufferedImage _bitmap = null;
	private BufferedImage _isometricImage = null;
	private byte[] pixels;
	private byte[] pixels_iso;

	public Sprite(int w, int h, IndexColorModel colorModel)
	{
		_bitmap = new BufferedImage(w, h, BufferedImage.TYPE_BYTE_INDEXED, colorModel);
		_isometricImage = new BufferedImage(w, h + h / 2, BufferedImage.TYPE_BYTE_INDEXED, colorModel);
		pixels = ((DataBufferByte) _bitmap.getRaster().getDataBuffer()).getData();
		pixels_iso = ((DataBufferByte) _isometricImage.getRaster().getDataBuffer()).getData();
		for (int i = 0; i < w * h; i++) {
			pixels[i] = 16;
		}
		for (int i = 0; i < w * (h + h / 2); i++) {
			pixels_iso[i] = 16;
		}
	}

	/* (non-Javadoc)
	 * @see spriteEdit.IPainting#SetPixel(int, int, byte)
	 */
	public void SetPixel(int x, int y, byte color)
	{
		if (x >= 32 || x < 0) {
			System.out.println("Noe gikk galt");
		}
		if (y >= 32 || y < 0) {
			System.out.println("Noe gikk galt");
		}

		pixels[y * _bitmap.getWidth() + x] = color;
		int y0 = (_bitmap.getHeight() / 2 + y - x / 2);
		pixels_iso[y0 * _bitmap.getWidth() + x] = color;

	}

	/* (non-Javadoc)
	 * @see spriteEdit.IPainting#GetBitmap()
	 */
	public byte[] GetBitmap()
	{
		return ((DataBufferByte) _bitmap.getRaster().getDataBuffer()).getData();
	}

	/* (non-Javadoc)
	 * @see spriteEdit.IPainting#GetImage()
	 */
	public BufferedImage GetImage()
	{
		return _bitmap;
	}

	/* (non-Javadoc)
	 * @see spriteEdit.IPainting#GetIsometricImage(java.lang.Boolean)
	 */
	public BufferedImage GetIsometricImage(Boolean angle)
	{
		return _isometricImage;
	}

	/* (non-Javadoc)
	 * @see spriteEdit.IPainting#GetPixel(int, int)
	 */
	public byte GetPixel(int x, int y)
	{
		byte[] pixels = ((DataBufferByte) _bitmap.getRaster().getDataBuffer()).getData();
		return pixels[y * _bitmap.getWidth() + x];
	}

	public void Save(String filename)
	{
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see spriteEdit.IPainting#Clear()
	 */
	public void Clear()
	{
		for (int i = 0; i < _bitmap.getHeight(); i++) {
			for (int j = 0; j < _bitmap.getWidth(); j++) {
				SetPixel(j, i, (byte) 16);
			}
		}
	}

	/* (non-Javadoc)
	 * @see spriteEdit.IPainting#LoadTile(int, int)
	 */
	public void LoadTile(int tilex, int tiley)
	{

	}

}
